package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.ChatMember;
import com.epam.nichyparenka.chat.server.entity.User;
import com.epam.nichyparenka.chat.server.entity.UserRole;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("jdbc")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "classpath:applicationContextTest.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ChatMemberDaoJdbcTest {
    private static final int CHAT_MEMBER_ID = 2;
    private static final int CHAT_ID = 1;
    private static final int USER_ID = 1;
    private static final int INCORRECT_ID = -100;
    private static final UserRole ROLE_MEMBER = UserRole.MEMBER;
    private static final UserRole ROLE_ADMIN = UserRole.ADMIN;

    @Autowired
    private ChatMemberDaoJdbc chatMemberDao;

    @Test
    void createTrue() {
        ChatMember chatMember = ChatMember.builder()
                .chat(new Chat(CHAT_ID))
                .user(new User(USER_ID))
                .role(ROLE_MEMBER)
                .build();
        assertTrue(chatMemberDao.create(chatMember));
        ChatMember chatMemberFound = chatMemberDao.read(CHAT_MEMBER_ID + 1);
        assertEquals(ROLE_MEMBER, chatMemberFound.getRole());
    }

    @Test
    void createFalse_EmptyEntity() {
        ChatMember chatMember = ChatMember.builder()
                .chat(new Chat())
                .user(new User())
                .build();
        assertThrows(DataIntegrityViolationException.class, () -> chatMemberDao.create(chatMember));
    }

    @Test
    void readTrue() {
        assertNotNull(chatMemberDao.read(CHAT_MEMBER_ID));
    }

    @Test
    void readFalse_IncorrectId() {
        assertThrows(EmptyResultDataAccessException.class, () -> chatMemberDao.read(INCORRECT_ID));
    }

    @Test
    void readAllByChatIdTrue() {
        List<ChatMember> chatMembers = chatMemberDao.readAllByChatId(CHAT_ID);
        assertFalse(chatMembers.isEmpty());
    }

    @Test
    void readAllByChatIdFalse_IncorrectId() {
        List<ChatMember> chatMembers = chatMemberDao.readAllByChatId(INCORRECT_ID);
        assertTrue(chatMembers.isEmpty());
    }

    @Test
    void updateTrue() {
        ChatMember chatMember = ChatMember.builder()
                .id(CHAT_MEMBER_ID)
                .chat(new Chat(CHAT_ID))
                .user(new User(USER_ID))
                .role(ROLE_MEMBER)
                .build();
        chatMemberDao.create(chatMember);
        chatMember.setRole(ROLE_ADMIN);
        assertTrue(chatMemberDao.update(chatMember));
        ChatMember chatMemberFound = chatMemberDao.read(CHAT_MEMBER_ID);
        assertNotNull(chatMemberFound);
        assertEquals(chatMemberFound.getUser().getId(), CHAT_ID);
    }

    @Test
    void updateFalse_IncorrectId() {
        ChatMember chatMember = ChatMember.builder()
                .id(INCORRECT_ID)
                .chat(new Chat(CHAT_ID))
                .user(new User(USER_ID))
                .role(ROLE_MEMBER)
                .build();
        assertFalse(chatMemberDao.update(chatMember));
    }

    @Test
    void updateFalse_NotFullData() {
        ChatMember chatMember = ChatMember.builder()
                .chat(new Chat(CHAT_ID))
                .user(new User(USER_ID))
                .role(ROLE_ADMIN)
                .build();
        chatMemberDao.create(chatMember);
        chatMember.setChat(new Chat());
        chatMember.setUser(new User());
        assertFalse(chatMemberDao.update(chatMember));
    }

    @Test
    void deleteTrue() {
        assertTrue(chatMemberDao.delete(CHAT_MEMBER_ID));
        assertThrows(EmptyResultDataAccessException.class, () -> chatMemberDao.read(CHAT_MEMBER_ID));
    }

    @Test
    void deleteFalse_IncorrectId() {
        assertFalse(chatMemberDao.delete(INCORRECT_ID));
    }
}