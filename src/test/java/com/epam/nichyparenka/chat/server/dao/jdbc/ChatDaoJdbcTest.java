package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.entity.Chat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("jdbc")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "classpath:applicationContextTest.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ChatDaoJdbcTest {
    private static final int CHAT_ID = 1;
    private static final int USER_ID = 1;
    private static final String CHAT_NAME_1 = "Employees";
    private static final String CHAT_NAME_2 = "EPAM employees";
    private static final int INCORRECT_ID = -100;

    @Autowired
    private ChatDaoJdbc chatDao;

    @Test
    void createTrue() {
        Chat chat = new Chat(0, CHAT_NAME_1);
        assertTrue(chatDao.create(chat));
        Chat chatFound = chatDao.read(CHAT_ID + 1);
        assertEquals(CHAT_NAME_1, chatFound.getName());
    }

    @Test
    void createFalse_EmptyEntity() {
        assertThrows(DataIntegrityViolationException.class, () -> chatDao.create(new Chat()));
    }

    @Test
    void readTrue() {
        assertNotNull(chatDao.read(CHAT_ID));
    }

    @Test
    void readFalse_IncorrectId() {
        assertThrows(EmptyResultDataAccessException.class, () -> chatDao.read(INCORRECT_ID));
    }

    @Test
    void updateTrue() {
        Chat chat = new Chat(CHAT_ID, CHAT_NAME_2);
        assertTrue(chatDao.update(chat));
        Chat chatFound = chatDao.read(CHAT_ID);
        assertEquals(chatFound.getName(), CHAT_NAME_2);
    }

    @Test
    void updateFalse_IncorrectId() {
        Chat chat = new Chat(INCORRECT_ID, CHAT_NAME_1);
        assertFalse(chatDao.update(chat));
    }

    @Test
    void updateFalse_NotFullData() {
        assertThrows(DataIntegrityViolationException.class, () -> chatDao.update(new Chat(CHAT_ID)));
    }

    @Test
    void deleteTrue() {
        Chat chat = new Chat(CHAT_ID, CHAT_NAME_1);
        chatDao.create(chat);
        assertTrue(chatDao.delete(CHAT_ID + 1));
        assertThrows(EmptyResultDataAccessException.class, () -> chatDao.read(CHAT_ID + 1));
    }

    @Test
    void deleteFalse_IncorrectId() {
        assertFalse(chatDao.delete(INCORRECT_ID));
    }

    @Test
    void readAllByUserIdTrue() {
        List<Chat> chats = chatDao.readAllByUserId(USER_ID);
        assertTrue(chats.size() > 0);
    }

    @Test
    void readAllByUserIdFalse_IncorrectId() {
        List<Chat> chats = chatDao.readAllByUserId(INCORRECT_ID);
        assertFalse(chats.size() > 0);
    }
}