package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("jdbc")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "classpath:applicationContextTest.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserDaoJdbcTest {
    private static String USERNAME = "dima123";
    private static final String PASSWORD = "first password";
    private static final String FIRST_NAME_1 = "Dmitry";
    private static final String FIRST_NAME_2 = "Peter";
    private static final String LAST_NAME = "Nichiporenko";
    private static final String USERNAME_UPDATE_1 = "1";
    private static final int USER_ID = 2;
    private static final int INCORRECT_ID = -100;

    @Autowired
    private UserDaoJdbc userDao;

    @BeforeEach
    void beforeEach() {
        USERNAME += USERNAME_UPDATE_1;
    }

    @Test
    void createTrue() {
        User user = User.builder()
                .username(USERNAME)
                .password(PASSWORD)
                .firstName(FIRST_NAME_1)
                .lastName(LAST_NAME)
                .build();
        assertTrue(userDao.create(user));
        User userFound = userDao.read(USER_ID + 1);
        assertEquals(USERNAME, userFound.getUsername());
    }

    @Test
    void createFalse_EmptyEntity() {
        assertThrows(DataIntegrityViolationException.class, () -> userDao.create(new User()));
    }

    @Test
    void readTrue() {
        assertNotNull(userDao.read(USER_ID));
    }

    @Test
    void readFalse_IncorrectId() {
        assertThrows(EmptyResultDataAccessException.class, () -> userDao.read(INCORRECT_ID));
    }

    @Test
    void updateTrue() {
        User user = User.builder()
                .id(USER_ID + 1)
                .username(USERNAME)
                .password(PASSWORD)
                .firstName(FIRST_NAME_1)
                .lastName(LAST_NAME)
                .build();
        userDao.create(user);
        user.setFirstName(FIRST_NAME_2);
        assertTrue(userDao.update(user));
        User userFound = userDao.read(USER_ID + 1);
        assertEquals(FIRST_NAME_2, userFound.getFirstName());
    }

    @Test
    void updateFalse_IncorrectId() {
        User user = User.builder()
                .id(INCORRECT_ID)
                .username(USERNAME)
                .password(PASSWORD)
                .firstName(FIRST_NAME_1)
                .lastName(LAST_NAME)
                .build();
        assertFalse(userDao.update(user));
    }

    @Test
    void updateFalse_NotFullData() {
        User user = User.builder()
                .id(USER_ID)
                .build();
        assertFalse(userDao.update(user));
    }

    @Test
    void deleteTrue() {
        User user = User.builder()
                .id(USER_ID)
                .username(USERNAME)
                .password(PASSWORD)
                .firstName(FIRST_NAME_1)
                .lastName(LAST_NAME)
                .build();
        userDao.create(user);
        assertTrue(userDao.delete(USER_ID + 1));
        assertThrows(EmptyResultDataAccessException.class, () -> userDao.read(USER_ID + 1));
    }

    @Test
    void deleteFalse_IncorrectId() {
        assertFalse(userDao.delete(INCORRECT_ID));
    }
}