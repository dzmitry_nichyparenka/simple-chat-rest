package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.dao.ChatMemberDao;
import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.dto.ChatMemberDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.ChatMember;
import com.epam.nichyparenka.chat.server.entity.User;
import com.epam.nichyparenka.chat.server.entity.UserRole;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class ChatMemberServiceImplTest {
    private static final int CHAT_ID = 1;
    private static final int USER_ID = 1;
    private static final int CHAT_MEMBER_ID = 3;
    private static final int INCORRECT_ID = -100;
    private static final UserRole ROLE_MEMBER = UserRole.MEMBER;
    private static final String USERNAME = "dima123456";
    private static final String PASSWORD = "qwerty";
    private static final String FIRST_NAME = "Dzmitry";
    private static final String LAST_NAME = "Nichyparenka";
    private static final String CHAT_NAME = "CHATNAME";
    private static ChatMemberDto chatMemberDto;
    private static ChatMember chatMember;
    private static User user;
    private static Chat chat;

    @Mock
    private ChatMemberDao chatMemberDao;

    @Mock
    private ChatDao chatDao;

    @Mock
    private UserDao userDao;

    @InjectMocks
    private ChatMemberServiceImpl chatMemberService;

    @BeforeAll
    static void beforeAll() {
        user = User.builder().id(USER_ID)
                .username(USERNAME)
                .password(PASSWORD)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME).build();
        chat = Chat.builder().id(CHAT_ID)
                .name(CHAT_NAME).build();
        chatMember = ChatMember.builder().id(CHAT_MEMBER_ID)
                .chat(chat)
                .user(user)
                .role(ROLE_MEMBER).build();
        chatMemberDto = ChatMemberDto.builder().chatId(CHAT_MEMBER_ID)
                .chatId(CHAT_ID)
                .userId(USER_ID)
                .role(ROLE_MEMBER).build();
    }

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createTrue() {
        when(chatMemberDao.create(any())).thenReturn(true);
        assertTrue(chatMemberService.create(chatMemberDto));
    }

    @Test
    void createFalse_IntegrityViolation() {
        when(chatMemberDao.create(any())).thenThrow(DataIntegrityViolationException.class);
        assertThrows(DataIntegrityViolationException.class, () -> chatMemberService.create(chatMemberDto));
    }

    @Test
    void readTrue() {
        when(chatMemberDao.read(CHAT_MEMBER_ID)).thenReturn(chatMember);
        when(userDao.read(USER_ID)).thenReturn(user);
        when(chatDao.read(CHAT_ID)).thenReturn(chat);
        ChatMemberDto chatMember = chatMemberService.read(CHAT_MEMBER_ID);
        assertNotNull(chatMember);
    }

    @Test
    void readFalse_IncorrectId() {
        when(chatMemberDao.read(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> chatMemberService.read(INCORRECT_ID));
    }

    @Test
    void readAllByChatIdTrue() {
        List<ChatMember> chatMember = new ArrayList<>();
        chatMember.add(ChatMemberServiceImplTest.chatMember);
        when(chatMemberDao.readAllByChatId(CHAT_ID)).thenReturn(chatMember);
        when(userDao.read(USER_ID)).thenReturn(user);
        when(chatDao.read(CHAT_ID)).thenReturn(new Chat());
        List<ChatMemberDto> chatMembers = chatMemberService.readAllByChatId(CHAT_ID);
        assertFalse(chatMembers.isEmpty());
    }

    @Test
    void readAllByChatIdFalse_IncorrectId() {
        when(chatMemberDao.readAllByChatId(CHAT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> chatMemberService.readAllByChatId(INCORRECT_ID));
    }

    @Test
    void updateTrue() {
        when(chatMemberDao.update(any())).thenReturn(true);
        assertTrue(chatMemberService.update(chatMemberDto));
    }

    @Test
    void updateFalse() {
        when(chatMemberDao.update(any())).thenReturn(false);
        assertThrows(EmptyResultDataAccessException.class, () -> chatMemberService.update(chatMemberDto));
    }

    @Test
    void deleteTrue() {
        when(chatMemberDao.delete(CHAT_MEMBER_ID)).thenReturn(true);
        assertTrue(chatMemberService.delete(CHAT_MEMBER_ID));
    }

    @Test
    void deleteFalse_IncorrectId() {
        when(chatMemberDao.delete(INCORRECT_ID)).thenReturn(false);
        assertThrows(EmptyResultDataAccessException.class, () -> chatMemberService.delete(INCORRECT_ID));
    }
}