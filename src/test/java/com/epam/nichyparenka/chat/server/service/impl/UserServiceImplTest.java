package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.dto.UserDto;
import com.epam.nichyparenka.chat.server.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class UserServiceImplTest {
    private static String USERNAME = "dima123";
    private static final String PASSWORD_1 = "first password";
    private static final String FIRST_NAME = "Dmitry";
    private static final String LAST_NAME = "Nichiporenko";
    private static final String USERNAME_UPDATE_1 = "1";
    private static final int USER_ID = 3;
    private static final int INCORRECT_ID = -100;
    private static UserDto userDto;
    private static User user;

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeAll
    static void beforeAll() {
        userDto = UserDto.builder().userId(USER_ID)
                .username(USERNAME)
                .password(PASSWORD_1)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME).build();
        user = User.builder().id(USER_ID)
                .username(USERNAME)
                .password(PASSWORD_1)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .build();
    }

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        userDto.setUsername(userDto.getUsername() + USERNAME_UPDATE_1);
    }

    @Test
    void createTrue() {
        when(userDao.create(any())).thenReturn(true);
        assertTrue(userService.create(userDto));
    }

    @Test
    void createFalse() {
        when(userDao.create(any())).thenThrow(DataIntegrityViolationException.class);
        assertThrows(DataIntegrityViolationException.class, () -> userService.create(userDto));
    }

    @Test
    void readTrue() {
        when(userDao.read(USER_ID)).thenReturn(user);
        UserDto user = userService.read(USER_ID);
        assertNotNull(user);
    }

    @Test
    void readFalse() {
        when(userDao.read(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> userService.read(INCORRECT_ID));
    }

    @Test
    void updateTrue() {
        when(userDao.update(any())).thenReturn(true);
        assertTrue(userService.update(userDto));
    }

    @Test
    void updateFalse() {
        when(userDao.update(any())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> userService.update(userDto));
    }

    @Test
    void deleteTrue() {
        when(userDao.delete(USER_ID)).thenReturn(true);
        assertTrue(userService.delete(USER_ID));
    }

    @Test
    void deleteFalse() {
        when(userDao.delete(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> userService.delete(INCORRECT_ID));
    }
}