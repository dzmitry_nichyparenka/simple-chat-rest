package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.dao.MessageDao;
import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.dto.MessageDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.Message;
import com.epam.nichyparenka.chat.server.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class MessageServiceImplTest {
    private static final int MESSAGE_ID = 1;
    private static final int SENDER_ID = 1;
    private static final int CHAT_ID = 1;
    private static final int USER_ID = 1;
    private static final int INCORRECT_ID = -100;
    private static final String MESSAGE_TEXT = "Message text example";
    private static final String CHAT_NAME = "Employees";
    private static final LocalDateTime MESSAGE_TIME = LocalDateTime.of(2018, 7, 15, 10, 24, 15);
    private static final String USERNAME = "testUsername";
    private static final String PASSWORD = "testPassword";
    private static final String FIRST_NAME = "Dmitry";
    private static final String LAST_NAME = "Nichiporenko";
    private static MessageDto messageDto;
    private static Message message;
    private static User user;
    private static Chat chat;

    @Mock
    private MessageDao messageDao;

    @Mock
    private ChatDao chatDao;

    @Mock
    private UserDao userDao;

    @InjectMocks
    private MessageServiceImpl messageService;

    @BeforeAll
    static void beforeAll() {
        messageDto = MessageDto.builder().messageId(MESSAGE_ID)
                .senderId(SENDER_ID)
                .chatId(CHAT_ID)
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME).build();
        chat = new Chat(CHAT_ID, CHAT_NAME);
        user = User.builder()
                .id(USER_ID)
                .username(USERNAME)
                .password(PASSWORD)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME).build();
        message = Message.builder().id(MESSAGE_ID)
                .sender(user)
                .chat(chat)
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME).build();
    }

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createTrue() {
        when(messageDao.create(any())).thenReturn(true);
        assertTrue(messageService.create(messageDto));
    }

    @Test
    void createFalse() {
        when(messageDao.create(any())).thenThrow(DataIntegrityViolationException.class);
        assertThrows(DataIntegrityViolationException.class, () -> messageService.create(messageDto));
    }

    @Test
    void readTrue() {
        when(messageDao.read(MESSAGE_ID)).thenReturn(message);
        when(chatDao.read(CHAT_ID)).thenReturn(chat);
        when(userDao.read(USER_ID)).thenReturn(user);
        assertNotNull(messageService.read(MESSAGE_ID));
    }

    @Test
    void readFalse_IncorrectId() {
        when(messageDao.read(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> messageService.read(INCORRECT_ID));
    }

    @Test
    void readAllByChatIdTrue() {
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(messageDao.readAllByChatId(CHAT_ID)).thenReturn(messageList);
        when(chatDao.read(CHAT_ID)).thenReturn(chat);
        when(userDao.read(USER_ID)).thenReturn(user);
        assertFalse(messageService.readAllByChatId(CHAT_ID).isEmpty());
    }

    @Test
    void readAllByChatIdFalse_IncorrectId() {
        when(messageDao.readAllByChatId(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> messageService.readAllByChatId(INCORRECT_ID));
    }

    @Test
    void updateTrue() {
        when(messageDao.update(any())).thenReturn(true);
        assertTrue(messageService.update(messageDto));
    }

    @Test
    void updateFalse_IncorrectId() {
        when(messageDao.update(any())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> messageService.update(messageDto));
    }

    @Test
    void deleteTrue() {
        when(messageDao.delete(MESSAGE_ID)).thenReturn(true);
        assertTrue(messageService.delete(MESSAGE_ID));
    }

    @Test
    void deleteFalse_IncorrectId() {
        when(messageDao.delete(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> messageService.delete(INCORRECT_ID));
    }
}