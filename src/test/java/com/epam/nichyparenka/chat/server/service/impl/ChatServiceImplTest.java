package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.dto.ChatDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class ChatServiceImplTest {
    private static final String CHAT_NAME_1 = "Employees";
    private static final int CHAT_ID = 2;
    private static final int INCORRECT_ID = -100;
    private static ChatDto chatDto = new ChatDto();
    private static Chat chat = new Chat();

    @Mock
    private ChatDao chatDao;

    @InjectMocks
    private ChatServiceImpl chatService;

    @BeforeAll
    static void beforeAll() {
        chat.setId(CHAT_ID);
        chat.setName(CHAT_NAME_1);
        chatDto.setChatId(CHAT_ID);
        chatDto.setChatName(CHAT_NAME_1);
    }

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createTrue() {
        when(chatDao.create(any())).thenReturn(true);
        assertTrue(chatService.create(chatDto));
    }

    @Test
    void createFalse() {
        when(chatDao.create(any())).thenReturn(false);
        assertFalse(chatService.create(chatDto));
    }

    @Test
    void readTrue() {
        when(chatDao.read(CHAT_ID)).thenReturn(chat);
        assertNotNull(chatService.read(CHAT_ID));
    }

    @Test
    void readFalse_IncorrectId() {
        when(chatDao.read(INCORRECT_ID)).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(EmptyResultDataAccessException.class, () -> chatService.read(INCORRECT_ID));
    }

    @Test
    void updateTrue() {
        when(chatDao.update(any())).thenReturn(true);
        assertTrue(chatService.update(chatDto));
    }

    @Test
    void updateFalse_IncorrectId() {
        when(chatDao.update(any())).thenReturn(false);
        assertThrows(EmptyResultDataAccessException.class, () -> chatService.update(chatDto));
    }

    @Test
    void deleteTrue() {
        when(chatDao.delete(CHAT_ID)).thenReturn(true);
        assertTrue(chatService.delete(CHAT_ID));
    }

    @Test
    void deleteFalse_IncorrectId() {
        assertThrows(EmptyResultDataAccessException.class, () -> chatService.delete(INCORRECT_ID));
    }
}