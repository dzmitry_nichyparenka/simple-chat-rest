package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.Message;
import com.epam.nichyparenka.chat.server.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("jdbc")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "classpath:applicationContextTest.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class MessageDaoJdbcTest {
    private static final int MESSAGE_ID = 2;
    private static final int SENDER_ID = 1;
    private static final int CHAT_ID = 1;
    private static final int INCORRECT_ID = -100;
    private static final String MESSAGE_TEXT = "Message text example";
    private static final String MESSAGE_TEXT_2 = "The second message example";
    private static final LocalDateTime MESSAGE_TIME = LocalDateTime.of(2018, 7, 15, 10, 24, 15);

    @Autowired
    private MessageDaoJdbc messageDao;

    @Test
    void createTrue() {
        Message message = Message.builder()
                .chat(new Chat(CHAT_ID))
                .sender(new User(SENDER_ID))
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME)
                .build();
        assertTrue(messageDao.create(message));
        Message messageFound = messageDao.read(MESSAGE_ID + 1);
        assertEquals(CHAT_ID, messageFound.getChat().getId());
        assertEquals(SENDER_ID, messageFound.getSender().getId());
    }

    @Test
    void createFalse_EmptyEntity() {
        Message message = Message.builder()
                .chat(new Chat())
                .sender(new User())
                .build();
        assertThrows(DataIntegrityViolationException.class, () -> messageDao.create(message));
    }

    @Test
    void readTrue() {
        Message message = Message.builder()
                .chat(new Chat(CHAT_ID))
                .sender(new User(SENDER_ID))
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME)
                .build();
        assertTrue(messageDao.create(message));
        assertNotNull(messageDao.read(MESSAGE_ID));
    }

    @Test
    void readFalse_IncorrectId() {
        assertThrows(EmptyResultDataAccessException.class, () -> messageDao.read(INCORRECT_ID));
    }

    @Test
    void readAllByChatIdTrue() {
        List<Message> messages = messageDao.readAllByChatId(CHAT_ID);
        assertFalse(messages.isEmpty());
    }

    @Test
    void readAllByChatIdFalse_IncorrectId() {
        List<Message> messages = messageDao.readAllByChatId(INCORRECT_ID);
        assertTrue(messages.isEmpty());
    }

    @Test
    void updateTrue() {
        Message message = Message.builder()
                .id(MESSAGE_ID)
                .chat(new Chat(CHAT_ID))
                .sender(new User(SENDER_ID))
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME)
                .build();
        messageDao.create(message);
        message.setText(MESSAGE_TEXT_2);
        assertTrue(messageDao.update(message));
        Message messageFound = messageDao.read(MESSAGE_ID);
        assertEquals(messageFound.getChat().getId(), CHAT_ID);
    }

    @Test
    void updateFalse_IncorrectId() {
        Message message = Message.builder()
                .id(INCORRECT_ID)
                .chat(new Chat(CHAT_ID))
                .sender(new User(SENDER_ID))
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME)
                .build();
        assertFalse(messageDao.update(message));
    }

    @Test
    void updateFalse_NotFullData() {
        Message message = Message.builder()
                .id(MESSAGE_ID)
                .chat(new Chat())
                .sender(new User())
                .build();
        assertThrows(DataIntegrityViolationException.class, () -> messageDao.update(message));
    }

    @Test
    void deleteTrue() {
        Message message = Message.builder()
                .id(MESSAGE_ID)
                .chat(new Chat(CHAT_ID))
                .sender(new User(SENDER_ID))
                .text(MESSAGE_TEXT)
                .time(MESSAGE_TIME)
                .build();
        messageDao.create(message);
        assertTrue(messageDao.delete(MESSAGE_ID + 1));
        assertThrows(EmptyResultDataAccessException.class, () -> messageDao.read(MESSAGE_ID + 1));
    }

    @Test
    void deleteFalse_IncorrectId() {
        assertFalse(messageDao.delete(INCORRECT_ID));
    }
}