INSERT INTO "public".users (username, password, first_name, last_name) VALUES
  ('dmitry123', '$2a$04$8Uy9s4DhNjS58zhracf0/u5ron6EUQYyGDDcp72ioUFa5cgNPPyUS', 'Dmitry', 'Nichiporenko'),
  ('sergey', '$2a$04$Ohg71XezBuzqib3xhzvk0eYw.7gRi87TUqa7w89Ow//WbwdiFht2O', 'Sergey', 'Ivanov');

INSERT INTO "public".chats (name) VALUES ('Friends');

INSERT INTO "public".chat_members (user_id, chat_id, role) VALUES
  (1, 1, 'CREATOR'),
  (2, 1, 'MEMBER');

INSERT INTO "public".messages (sender_id, chat_id, text, time) VALUES
  (1, 1, 'Hi, my name is Dima!', '2018-07-15 10:24:15'),
  (2, 1, 'Hello! I am Sergey!', '2018-07-15 15:20:30');