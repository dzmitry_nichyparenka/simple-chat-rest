CREATE SCHEMA IF NOT EXISTS "public";

DROP TABLE IF EXISTS messages;
DROP TABLE IF EXISTS chat_members;
DROP TABLE IF EXISTS chats;
DROP TABLE IF EXISTS users;

DROP TYPE IF EXISTS user_role;
CREATE TYPE user_role AS ENUM ('CREATOR', 'ADMIN', 'MEMBER');

CREATE TABLE IF NOT EXISTS "public".users (
  id            bigserial       PRIMARY KEY,
  username      varchar(50)     NOT NULL UNIQUE,
  password      varchar(60)     NOT NULL,
  first_name    varchar(30)     NOT NULL,
  last_name     varchar(30)     NOT NULL
);

CREATE TABLE IF NOT EXISTS "public".chats (
  id            bigserial       PRIMARY KEY,
  name          varchar(50)     NOT NULL
);

CREATE TABLE IF NOT EXISTS "public".chat_members (
  id            bigserial       PRIMARY KEY,
  user_id       bigint          NOT NULL,
  chat_id       bigint          NOT NULL,
  role          user_role       NOT NULL
);

CREATE TABLE IF NOT EXISTS "public".messages (
  id            bigserial       PRIMARY KEY,
  sender_id     bigint          NOT NULL,
  chat_id       bigint          NOT NULL,
  text          varchar(500)    NOT NULL,
  time          timestamp       NOT NULL
);

ALTER TABLE "public".messages DROP CONSTRAINT IF EXISTS fk_messages_users;
ALTER TABLE "public".messages DROP CONSTRAINT IF EXISTS fk_messages_chats;
ALTER TABLE "public".chat_members DROP CONSTRAINT IF EXISTS fk_chat_members_users;
ALTER TABLE "public".chat_members DROP CONSTRAINT IF EXISTS fk_chat_members_chats;

ALTER TABLE "public".messages
  ADD CONSTRAINT fk_messages_users FOREIGN KEY (sender_id) REFERENCES "public".users (id) ON DELETE RESTRICT;
ALTER TABLE "public".messages
  ADD CONSTRAINT fk_messages_chats FOREIGN KEY (chat_id) references "public".chats (id) ON DELETE CASCADE;
ALTER TABLE "public".chat_members
  ADD CONSTRAINT fk_chat_members_users FOREIGN KEY (user_id) REFERENCES "public".users (id) ON DELETE RESTRICT;
ALTER TABLE "public".chat_members
  ADD CONSTRAINT fk_chat_members_chats FOREIGN KEY (chat_id) REFERENCES "public".chats (id) ON DELETE RESTRICT;