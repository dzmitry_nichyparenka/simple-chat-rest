package com.epam.nichyparenka.chat.server.resource.mapper.jdbc;

import org.springframework.dao.EmptyResultDataAccessException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Mapper for situations, when zero rows (or elements) were actually returned.
 *
 * @author Dzmitry Nichyparenka
 */
@Provider
public class EmptyResultMapper implements ExceptionMapper<EmptyResultDataAccessException> {
    private static final String MESSAGE = "Nothing found.";

    @Override
    public Response toResponse(EmptyResultDataAccessException exception) {
        return Response.status(404).entity(MESSAGE).type(MediaType.TEXT_PLAIN).build();
    }
}
