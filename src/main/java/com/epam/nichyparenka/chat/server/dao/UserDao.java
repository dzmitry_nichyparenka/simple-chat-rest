package com.epam.nichyparenka.chat.server.dao;

import com.epam.nichyparenka.chat.server.entity.User;

/**
 * The interface contains all necessary abstract methods (basic CRUD and
 * other necessary) and it must be implemented by all classes which are
 * intended to play the role of DAOs for Users.
 *
 * @author Dzmitry Nichyparenka
 */
public interface UserDao extends BasicDao<User> {

    /**
     * The implementation of the method reads the user by username.
     *
     * @param username username
     * @return user entity
     */
    User readByUsername(String username);
}
