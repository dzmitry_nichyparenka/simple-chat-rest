package com.epam.nichyparenka.chat.server.entity;

import lombok.*;

import java.time.LocalDateTime;

/**
 * The class represents the entity which contains all data of the message.
 *
 * @author Dzmitry Nichyparenka
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Message extends BasicEntity {
    private User sender;
    private Chat chat;
    private String text;
    private LocalDateTime time;

    /**
     * The constructor creates message entity with id.
     *
     * @param id id of the message
     */
    public Message(long id) {
        super(id);
    }

    /**
     * The constructor creates the fully completed message entity.
     *
     * @param id     id of the message
     * @param sender sender of the message
     * @param chat   chat of the message
     * @param text   text of the message
     * @param time   sending time of the message
     */
    @Builder
    public Message(long id, User sender, Chat chat, String text, LocalDateTime time) {
        super(id);
        this.sender = sender;
        this.chat = chat;
        this.text = text;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + getId() +
                ", sender=" + sender +
                ", chat=" + chat +
                ", text='" + text + '\'' +
                ", time=" + time +
                '}';
    }
}
