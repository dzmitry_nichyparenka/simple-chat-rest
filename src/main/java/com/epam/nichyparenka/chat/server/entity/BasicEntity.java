package com.epam.nichyparenka.chat.server.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class provides basic entity. All entities must be inherited
 * from this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class BasicEntity {
    protected long id;
}
