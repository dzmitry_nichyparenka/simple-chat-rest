package com.epam.nichyparenka.chat.server.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data Transfer Object for users.
 *
 * @author Dzmitry Nichyparenka
 */
@Data
@NoArgsConstructor
@XmlRootElement(name = "User")
public class UserDto implements BasicDto {
    private long userId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;

    /**
     * The constructor creates user DTO with id.
     *
     * @param userId id of the user
     */
    public UserDto(long userId) {
        this.userId = userId;
    }

    /**
     * The constructor creates the fully completed user DTO.
     *
     * @param userId    id of the user
     * @param username  username
     * @param password  password of the user
     * @param firstName first name of the user
     * @param lastName  last name of the user
     */
    @Builder
    public UserDto(long userId, String username, String password, String firstName, String lastName) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
