package com.epam.nichyparenka.chat.server.resource.mapper.jdbc;

import org.springframework.dao.DataIntegrityViolationException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Mapper for situations, when an attempt to insert or update data results in violation
 * of an integrity constraint.
 *
 * @author Dzmitry Nichyparenka
 */
@Provider
public class DataIntegrityViolationMapper implements ExceptionMapper<DataIntegrityViolationException> {
    private static final String MESSAGE = "Action failed due to data integrity violation (bad parameters).";

    @Override
    public Response toResponse(DataIntegrityViolationException exception) {
        // HTTP status code 422 - Unprocessable Entity
        return Response.status(422).entity(MESSAGE).type(MediaType.TEXT_PLAIN).build();
    }
}
