package com.epam.nichyparenka.chat.server.util;

import com.epam.nichyparenka.chat.server.dto.ChatDto;
import com.epam.nichyparenka.chat.server.dto.ChatMemberDto;
import com.epam.nichyparenka.chat.server.dto.MessageDto;
import com.epam.nichyparenka.chat.server.dto.UserDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.ChatMember;
import com.epam.nichyparenka.chat.server.entity.Message;
import com.epam.nichyparenka.chat.server.entity.User;

/**
 * Class contains methods for conversion from Entity into DTO.
 *
 * @author Dzmitry Nichyparenka
 */
public abstract class EntityConverter {

    /**
     * Method converts User Entity to User DTO.
     *
     * @param user user entity
     * @return user DTO
     */
    public static UserDto convertUserEntityToDTO(User user) {
        if (user == null) {
            return new UserDto();
        }
        return UserDto.builder()
                .userId(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

    /**
     * Method converts Chat Entity to Chat DTO.
     *
     * @param chat chat entity
     * @return chat DTO
     */
    public static ChatDto convertChatEntityToDTO(Chat chat) {
        if (chat == null) {
            return new ChatDto();
        }
        return ChatDto.builder()
                .chatId(chat.getId())
                .chatName(chat.getName())
                .build();
    }

    /**
     * Method converts Chat Member Entity to Chat Member DTO with id.
     *
     * @param chatMember chat member entity
     * @return chat member DTO
     */
    public static ChatMemberDto convertChatMemberEntityToDtoWithId(ChatMember chatMember) {
        if (chatMember == null) {
            return new ChatMemberDto();
        }
        ChatMemberDto chatMemberDto = ChatMemberDto.builder()
                .chatMemberId(chatMember.getId())
                .role(chatMember.getRole())
                .build();
        Chat chat = chatMember.getChat();
        User user = chatMember.getUser();
        if (chat == null) {
            chatMemberDto.setChatId(0);
        } else {
            chatMemberDto.setChatId(chat.getId());
        }
        if (user == null) {
            chatMemberDto.setUserId(0);
        } else {
            chatMemberDto.setUserId(user.getId());
        }
        return chatMemberDto;
    }

    /**
     * Method converts Chat Member Entity to Chat Member DTO fully.
     *
     * @param chatMember chat member entity
     * @return chat member DTO
     */
    public static ChatMemberDto convertChatMemberEntityToDtoFully(ChatMember chatMember) {
        if (chatMember == null) {
            return new ChatMemberDto();
        }
        User user = chatMember.getUser();
        Chat chat = chatMember.getChat();
        return ChatMemberDto.builder()
                .chatMemberId(chatMember.getId())
                .role(chatMember.getRole())
                .chatId(chat.getId())
                .userId(user.getId())
                .chatName(chat.getName())
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

    /**
     * Method converts Message Entity to MessageDto with ids.
     *
     * @param message message entity
     * @return message DTO
     */
    public static MessageDto convertMessageEntityToDtoWithId(Message message) {
        if (message == null) {
            return new MessageDto();
        }
        MessageDto messageDto = MessageDto.builder()
                .messageId(message.getId())
                .text(message.getText())
                .time(message.getTime())
                .build();
        Chat chat = message.getChat();
        User sender = message.getSender();
        if (chat == null) {
            messageDto.setChatId(0);
        } else {
            messageDto.setChatId(message.getChat().getId());
        }
        if (sender == null) {
            messageDto.setSenderId(0);
        } else {
            messageDto.setSenderId(message.getSender().getId());
        }
        return messageDto;
    }

    /**
     * Method converts Message Entity to MessageDto fully.
     *
     * @param message message entity
     * @return message DTO
     */
    public static MessageDto convertMessageEntityToDtoFully(Message message) {
        if (message == null) {
            return new MessageDto();
        }
        Chat chat = message.getChat();
        User sender = message.getSender();
        return MessageDto.builder()
                .messageId(message.getId())
                .text(message.getText())
                .time(message.getTime())
                .chatId(chat.getId())
                .chatName(chat.getName())
                .senderId(sender.getId())
                .username(sender.getUsername())
                .build();
    }
}
