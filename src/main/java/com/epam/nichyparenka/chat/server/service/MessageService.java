package com.epam.nichyparenka.chat.server.service;

import com.epam.nichyparenka.chat.server.dto.MessageDto;

import java.util.List;

/**
 * The interface provides specific abstract methods for the service,
 * which contains the main logic associated with messages in the application.
 *
 * @author Dzmitry Nichyparenka
 */
public interface MessageService extends BasicService<MessageDto> {

    /**
     * The implementation of the method reads all messages in the specified chat.
     *
     * @param chatId chat record id
     * @return list of found chats
     */
    List<MessageDto> readAllByChatId(long chatId);

}
