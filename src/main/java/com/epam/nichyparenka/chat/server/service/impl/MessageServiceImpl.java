package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.dao.MessageDao;
import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.dto.MessageDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.Message;
import com.epam.nichyparenka.chat.server.entity.User;
import com.epam.nichyparenka.chat.server.service.MessageService;
import com.epam.nichyparenka.chat.server.util.DtoConverter;
import com.epam.nichyparenka.chat.server.util.EntityConverter;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Class provides methods with the main logic associated with messages in the
 * application.
 *
 * @author Dzmitry Nichyparenka
 */
public class MessageServiceImpl implements MessageService {
    private final MessageDao messageDao;
    private final ChatDao chatDao;
    private final UserDao userDao;

    public MessageServiceImpl(MessageDao messageDao, ChatDao chatDao, UserDao userDao) {
        this.messageDao = messageDao;
        this.chatDao = chatDao;
        this.userDao = userDao;
    }

    /**
     * The method creates a new message in accordance with the data in the provided DTO.
     * For this purpose, {@code MessageDao} object is used.
     *
     * @param messageDto the Data Transfer Object filled with data to create a new message
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(MessageDto messageDto) {
        Message message = DtoConverter.convertMessageDtoToEntityWithId(messageDto);
        return messageDao.create(message);
    }

    /**
     * The method reads the message in accordance with the provided identifier.
     * For this purpose, {@code MessageDao} object is used.
     *
     * @param id message record id
     * @return the Data Transfer Object filled with data of message
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {EmptyResultDataAccessException.class, DataAccessResourceFailureException.class})
    public MessageDto read(long id) {
        Message message = messageDao.read(id);
        Chat chat = chatDao.read(message.getChat().getId());
        User sender = userDao.read(message.getSender().getId());
        message.setChat(chat);
        message.setSender(sender);
        MessageDto messageDto = EntityConverter.convertMessageEntityToDtoFully(message);
        return messageDto;
    }

    /**
     * The method reads all messages in accordance with the provided chat identifier.
     * For this purpose, {@code MessageDao} object is used.
     *
     * @param chatId chat id
     * @return list of found messages
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {EmptyResultDataAccessException.class, DataAccessResourceFailureException.class})
    public List<MessageDto> readAllByChatId(long chatId) {
        List<Message> messagesFound = messageDao.readAllByChatId(chatId);
        Chat chat = chatDao.read(chatId);

        List<MessageDto> messagesDto = new ArrayList<>();

        // Convert Entities into DTOs
        messagesFound.forEach(message -> {
            User sender = userDao.read(message.getSender().getId());
            message.setChat(chat);
            message.setSender(sender);
            MessageDto messageDto = EntityConverter.convertMessageEntityToDtoFully(message);
            messagesDto.add(messageDto);
        });
        return messagesDto;
    }

    /**
     * The method updates the data of the message by id.
     * For this purpose, {@code MessageDao} object is used.
     *
     * @param messageDto the Data Transfer Object to update the message
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(MessageDto messageDto) {
        Message message = DtoConverter.convertMessageDtoToEntityWithId(messageDto);
        if (!messageDao.update(message)) {
            throw new EmptyResultDataAccessException(1);
        }
        return true;
    }

    /**
     * The method deletes the message in accordance with the provided identifier.
     * For this purpose, {@code MessageDao} object is used.
     *
     * @param id message record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (messageDao.delete(id)) {
            return true;
        }
        throw new EmptyResultDataAccessException(1);
    }
}
