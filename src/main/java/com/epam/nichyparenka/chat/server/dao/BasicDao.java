package com.epam.nichyparenka.chat.server.dao;

import com.epam.nichyparenka.chat.server.entity.BasicEntity;

/**
 * The interface contains all abstract CRUD methods and it must be implemented by all
 * classes which are intended to play the role of DAOs.
 *
 * @param <E> an entity of the type extended from {@code BasicEntity}
 * @author Dzmitry Nichyparenka
 */
public interface BasicDao<E extends BasicEntity> {
    /**
     * The implementation of the method creates the record in the corresponding database table with the data
     * in the provided entity.
     *
     * @param entity an entity filled with data to create a record in the database
     * @return true if the creation was successful, false otherwise
     */
    boolean create(E entity);

    /**
     * The implementation of the method reads the record from the corresponding database table.
     *
     * @param id record id
     * @return an entity with data
     */
    E read(long id);

    /**
     * The implementation of the method updates the record in the corresponding database table.
     *
     * @param entity an entity to update the record in the database
     * @return true if the update was successful, false otherwise
     */
    boolean update(E entity);

    /**
     * The implementation of the method deletes the record from the corresponding database table.
     *
     * @param id record id
     * @return true if the deletion was successful, false otherwise
     */
    boolean delete(long id);
}
