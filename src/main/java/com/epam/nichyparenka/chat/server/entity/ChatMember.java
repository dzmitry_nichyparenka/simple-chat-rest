package com.epam.nichyparenka.chat.server.entity;

import lombok.*;


/**
 * The class represents the entity which contains all data of the chat member.
 *
 * @author Dzmitry Nichyparenka
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ChatMember extends BasicEntity {
    private Chat chat;
    private User user;
    private UserRole role;

    /**
     * The constructor creates chat member entity with id.
     *
     * @param id id of the chat member
     */
    public ChatMember(long id) {
        super(id);
    }

    /**
     * The constructor creates the fully completed chat member entity.
     *
     * @param id   id of the chat member
     * @param chat chat object associated with this chat member
     * @param user user object associated with this chat member
     * @param role role in the chat of this chat member
     */
    @Builder
    public ChatMember(long id, Chat chat, User user, UserRole role) {
        super(id);
        this.chat = chat;
        this.user = user;
        this.role = role;
    }

    @Override
    public String toString() {
        return "ChatMember{" +
                "id=" + getId() +
                ", chat=" + chat +
                ", user=" + user +
                ", role=" + role +
                '}';
    }
}
