package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.dao.ChatMemberDao;
import com.epam.nichyparenka.chat.server.dto.ChatDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.service.ChatService;
import com.epam.nichyparenka.chat.server.util.DtoConverter;
import com.epam.nichyparenka.chat.server.util.EntityConverter;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Class provides methods with the main logic associated with chats in the application.
 *
 * @author Dzmitry Nichyparenka
 */
public class ChatServiceImpl implements ChatService {
    private final ChatDao chatDao;
    private final ChatMemberDao chatMemberDao;

    public ChatServiceImpl(ChatDao chatDao, ChatMemberDao chatMemberDao) {
        this.chatDao = chatDao;
        this.chatMemberDao = chatMemberDao;
    }

    /**
     * The method creates a new chat in accordance with the data in the provided DTO.
     *
     * @param chatDto the Data Transfer Object filled with data to create a new chat
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(ChatDto chatDto) {
        Chat chat = DtoConverter.convertChatDtoToEntity(chatDto);
        return chatDao.create(chat);
    }

    /**
     * The method reads the chat in accordance with the provided identifier.
     *
     * @param id chat record id
     * @return the Data Transfer Object filled with data of chat
     */
    @Override
    public ChatDto read(long id) {
        Chat chat = chatDao.read(id);
        return EntityConverter.convertChatEntityToDTO(chat);
    }

    /**
     * The method updates the data of the chat by id.
     *
     * @param chatDto the Data Transfer Object to update the chat
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(ChatDto chatDto) {
        Chat chat = DtoConverter.convertChatDtoToEntity(chatDto);
        if (!chatDao.update(chat)) {
            throw new EmptyResultDataAccessException(1);
        }
        return true;
    }

    /**
     * The method deletes the chat in accordance with the provided identifier.
     * For this purpose, {@code ChatDao} object is used.
     *
     * @param id chat record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (!chatDao.delete(id)) {
            throw new EmptyResultDataAccessException(1);
        } else {
            return true;
        }
    }

    /**
     * The method reads all chats with count of the members by username.
     *
     * @param userId id of the user
     * @return list of the chats
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {EmptyResultDataAccessException.class, DataAccessResourceFailureException.class})
    public List<ChatDto> readChatsByUserId(long userId) {
        List<Chat> chatsFound = chatDao.readAllByUserId(userId);

        List<ChatDto> chatsDto = new ArrayList<>();

        // Convert Entities into DTOs
        chatsFound.forEach(chat -> {
            int membersCount = chatMemberDao.readAllByChatId(chat.getId()).size();
            if (membersCount == 0) {
                throw new EmptyResultDataAccessException(1);
            }
            ChatDto chatDto = EntityConverter.convertChatEntityToDTO(chat);
            chatDto.setMembersCount(membersCount);
            chatsDto.add(chatDto);
        });
        return chatsDto;
    }
}
