package com.epam.nichyparenka.chat.server.entity;

/**
 * Enum provides different user roles, which can be assigned to users in chats.
 *
 * @author Dzmitry Nichyparenka
 */
public enum UserRole {
    CREATOR,
    ADMIN,
    MEMBER
}
