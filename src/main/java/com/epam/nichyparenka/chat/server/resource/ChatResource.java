package com.epam.nichyparenka.chat.server.resource;

import com.epam.nichyparenka.chat.server.dto.ChatDto;
import com.epam.nichyparenka.chat.server.dto.ChatMemberDto;
import com.epam.nichyparenka.chat.server.dto.MessageDto;
import com.epam.nichyparenka.chat.server.service.ChatMemberService;
import com.epam.nichyparenka.chat.server.service.ChatService;
import com.epam.nichyparenka.chat.server.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Class handles requests from clients associated with chats.
 *
 * @author Dzmitry Nichyparenka
 */
@Path("chats")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ChatResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatResource.class);
    private final ChatService chatService;
    private final MessageService messageService;
    private final ChatMemberService chatMemberService;

    @HeaderParam("Authorization")
    private String authorization;

    @Autowired
    public ChatResource(ChatService chatService, MessageService messageService, ChatMemberService chatMemberService) {
        this.chatService = chatService;
        this.messageService = messageService;
        this.chatMemberService = chatMemberService;
    }

    @GET
    @Path("{chatId}")
    public Response getChat(@PathParam("chatId") long chatId) {
        byte[] bytes = DatatypeConverter.parseBase64Binary(authorization);
        String usernamePassword = new String(bytes, StandardCharsets.UTF_8);

        ChatDto chat = chatService.read(chatId);
        LOGGER.info("[GET] chats/{}", chatId);
        return Response.ok(chat).build();
    }

    @POST
    public Response createChat(@RequestBody ChatDto chatDto) {
        if (chatService.create(chatDto)) {
            LOGGER.info("[POST] chats {}", chatDto);
            return Response.status(Response.Status.CREATED).build();
        }
        LOGGER.error("[POST] chats {}", chatDto);
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Path("{chatId}/messages")
    public Response getChatMessages(@PathParam("chatId") long chatId) {
        List<MessageDto> messages = messageService.readAllByChatId(chatId);
        LOGGER.info("[GET] chats/{}/messages", chatId);
        return Response.ok(messages).build();
    }

    @GET
    @Path("{chatId}/members")
    public Response getChatMembers(@PathParam("chatId") long chatId) {
        List<ChatMemberDto> members = chatMemberService.readAllByChatId(chatId);
        LOGGER.info("[GET] chats/{}/members", chatId);
        return Response.ok(members).build();
    }

    @POST
    @Path("{chatId}/messages")
    public Response sendMessage(@PathParam("chatId") long chatId, MessageDto messageDto) {
        messageDto.setChatId(chatId);
        if (messageService.create(messageDto)) {
            LOGGER.info("[POST] chats/{} {}", chatId, messageDto);
            return Response.status(Response.Status.CREATED).build();
        }
        LOGGER.error("[POST] chats/{} {}", chatId, messageDto);
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
