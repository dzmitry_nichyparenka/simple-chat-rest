package com.epam.nichyparenka.chat.server.resource;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Class provides Jersey configuration (registration of resources
 * and providers).
 *
 * @author Dzmitry Nichyparenka
 */
public class JerseyResourceConfig extends ResourceConfig {
    private static final String[] PACKAGES = {
            "com.epam.nichyparenka.chat.server.resource"
    };

    public JerseyResourceConfig() {
        packages(PACKAGES);
    }
}
