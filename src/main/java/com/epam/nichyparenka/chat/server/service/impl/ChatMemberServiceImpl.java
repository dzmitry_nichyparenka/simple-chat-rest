package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.dao.ChatMemberDao;
import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.dto.ChatMemberDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.ChatMember;
import com.epam.nichyparenka.chat.server.entity.User;
import com.epam.nichyparenka.chat.server.service.ChatMemberService;
import com.epam.nichyparenka.chat.server.util.DtoConverter;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.epam.nichyparenka.chat.server.util.EntityConverter.convertChatMemberEntityToDtoFully;

/**
 * Class provides methods with the main logic associated with chat members in the
 * application.
 *
 * @author Dzmitry Nichyparenka
 */
public class ChatMemberServiceImpl implements ChatMemberService {
    private final ChatMemberDao chatMemberDao;
    private final ChatDao chatDao;
    private final UserDao userDao;

    public ChatMemberServiceImpl(ChatMemberDao chatMemberDao, ChatDao chatDao, UserDao userDao) {
        this.chatMemberDao = chatMemberDao;
        this.chatDao = chatDao;
        this.userDao = userDao;
    }

    /**
     * The method creates a new chat member in accordance with the data in the provided DTO.
     * For this purpose, {@code ChatMemberDao} object is used.
     *
     * @param chatMemberDto the Data Transfer Object filled with data to create a new chat member
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(ChatMemberDto chatMemberDto) {
        ChatMember chatMember = DtoConverter.convertChatMemberDtoToEntityWithId(chatMemberDto);
        return chatMemberDao.create(chatMember);
    }

    /**
     * The method reads the chat member in accordance with the provided identifier.
     * For this purpose, {@code ChatMemberDao} object is used.
     *
     * @param id chat member id
     * @return the Data Transfer Object filled with data of chat member
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {EmptyResultDataAccessException.class, DataAccessResourceFailureException.class})
    public ChatMemberDto read(long id) {
        ChatMember chatMember = chatMemberDao.read(id);
        Chat chat = chatDao.read(chatMember.getChat().getId());
        User user = userDao.read(chatMember.getUser().getId());
        chatMember.setChat(chat);
        chatMember.setUser(user);
        return convertChatMemberEntityToDtoFully(chatMember);
    }

    /**
     * The method reads all chat members in accordance with the provided chat identifier.
     * For this purpose, {@code ChatMemberDao} object is used.
     *
     * @param chatId chat id
     * @return list of found chat members
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {EmptyResultDataAccessException.class, DataAccessResourceFailureException.class})
    public List<ChatMemberDto> readAllByChatId(long chatId) {
        List<ChatMember> membersFound = chatMemberDao.readAllByChatId(chatId);
        Chat chat = chatDao.read(chatId);

        List<ChatMemberDto> membersDto = new ArrayList<>();

        // Convert Entities into DTOs
        membersFound.forEach(chatMember -> {
            User user = userDao.read(chatMember.getUser().getId());
            chatMember.setChat(chat);
            chatMember.setUser(user);
            ChatMemberDto chatMemberDto = convertChatMemberEntityToDtoFully(chatMember);
            membersDto.add(chatMemberDto);
        });
        return membersDto;
    }

    /**
     * The method updates the data of the chat member by id.
     * For this purpose, {@code ChatMemberDao} object is used.
     *
     * @param chatMemberDto the Data Transfer Object to update the chat member
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(ChatMemberDto chatMemberDto) {
        ChatMember chatMember = DtoConverter.convertChatMemberDtoToEntityWithId(chatMemberDto);
        if (!chatMemberDao.update(chatMember)) {
            throw new EmptyResultDataAccessException(1);
        }
        return true;
    }

    /**
     * The method deletes the chat member in accordance with the provided identifier.
     * For this purpose, {@code ChatMemberDao} object is used.
     *
     * @param id chat member record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (!chatMemberDao.delete(id)) {
            throw new EmptyResultDataAccessException(1);
        }
        return true;
    }
}
