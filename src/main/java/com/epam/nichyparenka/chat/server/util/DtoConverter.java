package com.epam.nichyparenka.chat.server.util;

import com.epam.nichyparenka.chat.server.dto.ChatDto;
import com.epam.nichyparenka.chat.server.dto.ChatMemberDto;
import com.epam.nichyparenka.chat.server.dto.MessageDto;
import com.epam.nichyparenka.chat.server.dto.UserDto;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.ChatMember;
import com.epam.nichyparenka.chat.server.entity.Message;
import com.epam.nichyparenka.chat.server.entity.User;

/**
 * Class contains methods for conversion from DTO into Entity.
 *
 * @author Dzmitry Nichyparenka
 */
public abstract class DtoConverter {

    /**
     * Method converts User DTO to User Entity.
     *
     * @param userDto user DTO
     * @return user entity
     */
    public static User convertUserDtoToEntity(UserDto userDto) {
        if (userDto == null) {
            return new User();
        }
        return User.builder()
                .id(userDto.getUserId())
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .build();
    }

    /**
     * Method converts Chat DTO to Chat Entity.
     *
     * @param chatDto chat DTO
     * @return chat entity
     */
    public static Chat convertChatDtoToEntity(ChatDto chatDto) {
        if (chatDto == null) {
            return new Chat();
        }
        return new Chat(chatDto.getChatId(), chatDto.getChatName());
    }

    /**
     * Method converts Chat Member DTO to Chat Member Entity with ids only.
     *
     * @param chatMemberDto chat member DTO
     * @return chat member entity
     */
    public static ChatMember convertChatMemberDtoToEntityWithId(ChatMemberDto chatMemberDto) {
        if (chatMemberDto == null) {
            return new ChatMember();
        }
        return ChatMember.builder()
                .id(chatMemberDto.getChatMemberId())
                .chat(new Chat(chatMemberDto.getChatId()))
                .user(new User(chatMemberDto.getUserId()))
                .role(chatMemberDto.getRole())
                .build();
    }

    /**
     * Method converts Chat Member DTO to Chat Member Entity.
     *
     * @param chatMemberDto chat member DTO
     * @return chat member entity
     */
    public static ChatMember convertChatMemberDtoToEntityFully(ChatMemberDto chatMemberDto) {
        if (chatMemberDto == null) {
            return new ChatMember();
        }
        Chat chat = Chat.builder()
                .id(chatMemberDto.getChatId())
                .name(chatMemberDto.getChatName())
                .build();
        User user = User.builder()
                .id(chatMemberDto.getUserId())
                .username(chatMemberDto.getUsername())
                .firstName(chatMemberDto.getFirstName())
                .lastName(chatMemberDto.getLastName())
                .build();
        return ChatMember.builder()
                .id(chatMemberDto.getChatMemberId())
                .chat(chat)
                .user(user)
                .role(chatMemberDto.getRole())
                .build();
    }

    /**
     * Method converts Message DTO to Message Entity with ids only.
     *
     * @param messageDto message DTO
     * @return message entity
     */
    public static Message convertMessageDtoToEntityWithId(MessageDto messageDto) {
        if (messageDto == null) {
            return new Message();
        }
        return Message.builder()
                .id(messageDto.getMessageId())
                .chat(new Chat(messageDto.getChatId()))
                .sender(new User(messageDto.getSenderId()))
                .text(messageDto.getText())
                .time(messageDto.getTime())
                .build();
    }

    /**
     * Method converts Message DTO to Message Entity fully.
     *
     * @param messageDto message DTO
     * @return message entity
     */
    public static Message convertMessageDtoToEntityFully(MessageDto messageDto) {
        if (messageDto == null) {
            return new Message();
        }
        User sender = User.builder()
                .id(messageDto.getSenderId())
                .username(messageDto.getUsername())
                .build();
        Chat chat = Chat.builder()
                .id(messageDto.getChatId())
                .name(messageDto.getChatName())
                .build();
        return Message.builder()
                .id(messageDto.getMessageId())
                .chat(chat)
                .sender(sender)
                .text(messageDto.getText())
                .time(messageDto.getTime())
                .build();
    }
}
