package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.dao.ChatMemberDao;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.ChatMember;
import com.epam.nichyparenka.chat.server.entity.User;
import com.epam.nichyparenka.chat.server.entity.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * Class provides all methods for accessing the database table associated with
 * <b>Chat Members</b> using JDBC.
 *
 * @author Dzmitry Nichyparenka
 */
public class ChatMemberDaoJdbc implements ChatMemberDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatMemberDaoJdbc.class);
    private static final String SQL_CREATE_CHAT_MEMBER = "INSERT INTO \"public\".chat_members (user_id, chat_id, " +
            "role) VALUES (?, ?, ?)";
    private static final String SQL_READ_CHAT_MEMBER = "SELECT user_id, chat_id, role FROM \"public\".chat_members " +
            "WHERE id=?";
    private static final String SQL_READ_ALL_CHAT_MEMBERS = "SELECT id, user_id, role FROM \"public\".chat_members " +
            "WHERE chat_id=?";
    private static final String SQL_UPDATE_CHAT_MEMBER = "UPDATE \"public\".chat_members SET user_id=?, chat_id=?, " +
            "role=? WHERE id=?";
    private static final String SQL_DELETE_CHAT_MEMBER = "DELETE FROM \"public\".chat_members WHERE id=?";
    private static final String PARAMETER_ID = "id";
    private static final String PARAMETER_USER_ID = "user_id";
    private static final String PARAMETER_CHAT_ID = "chat_id";
    private static final String PARAMETER_ROLE = "role";
    private JdbcTemplate jdbcTemplate;

    public ChatMemberDaoJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * The method creates the record in the database table with chat members using
     * JDBC. The new record will be created with data from the object that is
     * passed to the parameter of this method.
     *
     * @param chatMember an entity filled with data to create a record in the database
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(ChatMember chatMember) {
        long userId = chatMember.getUser().getId();
        long chatId = chatMember.getChat().getId();
        UserRole role = chatMember.getRole();
        if (role == null) {
            throw new DataIntegrityViolationException("Role not specified");
        }

        if (jdbcTemplate.update(SQL_CREATE_CHAT_MEMBER, userId, chatId, role.toString()) == 1) {
            LOGGER.info("chat member (userId: {}, chatId: {})", userId, chatId);
            return true;
        }
        LOGGER.error("chat member (userId: {}, chatId: {})", userId, chatId);
        return false;
    }

    /**
     * The method reads the record from the database table with chat members using JDBC.
     *
     * @param id chat member record id
     * @return an entity with data
     */
    @Override
    public ChatMember read(long id) {
        return jdbcTemplate.queryForObject(SQL_READ_CHAT_MEMBER, new Object[]{id}, (resultSet, rowNum) -> {
            long userId = resultSet.getLong(PARAMETER_USER_ID);
            long chatId = resultSet.getLong(PARAMETER_CHAT_ID);
            UserRole role = UserRole.valueOf(resultSet.getString(PARAMETER_ROLE));

            ChatMember chatMember = ChatMember.builder()
                    .id(id)
                    .chat(new Chat(chatId))
                    .user(new User(userId))
                    .role(role)
                    .build();
            LOGGER.info("chat member (id: {}, userId: {}, chatId: {}, role: {})", id, userId, chatId, role);
            return chatMember;
        });
    }

    /**
     * The method reads all records from the database table with chat members,
     * which are belong to a specific chat.
     *
     * @param chatId chat record id
     * @return list of chat members found
     */
    @Override
    public List<ChatMember> readAllByChatId(long chatId) {
        List<ChatMember> chatMembers = jdbcTemplate.query(SQL_READ_ALL_CHAT_MEMBERS, new Object[]{chatId},
                (resultSet, rowNum) -> {
                    long chatMemberId = resultSet.getLong(PARAMETER_ID);
                    long userId = resultSet.getLong(PARAMETER_USER_ID);
                    UserRole role = UserRole.valueOf(resultSet.getString(PARAMETER_ROLE));

                    return ChatMember.builder()
                            .id(chatMemberId)
                            .user(new User(userId))
                            .chat(new Chat(chatId))
                            .role(role)
                            .build();
                });
        LOGGER.info("chat members (count: {})", chatMembers.size());
        return chatMembers;
    }

    /**
     * The method updates the record in the database table with chat members using JDBC.
     *
     * @param chatMember an entity to update the record
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(ChatMember chatMember) {
        User user = chatMember.getUser();
        Chat chat = chatMember.getChat();
        UserRole userRole = chatMember.getRole();
        long id = chatMember.getId();
        long userId = user.getId();
        long chatId = chat.getId();

        if (jdbcTemplate.update(SQL_UPDATE_CHAT_MEMBER, userId, chatId, userRole.toString(), id) == 1) {
            LOGGER.info("chat member (id: {}, userId: {}, chatId: {})", id, userId, chatId);
            return true;
        }
        LOGGER.error("chat member (id: {}, userId: {}, chatId: {})", id, userId, chatId);
        return false;
    }

    /**
     * The method deletes the record from the database table with chat members using
     * JPA implementation.
     *
     * @param id chat member record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (jdbcTemplate.update(SQL_DELETE_CHAT_MEMBER, id) == 1) {
            LOGGER.info("chat member (id: {})", id);
            return true;
        }
        LOGGER.error("chat member (id: {})", id);
        return false;
    }
}
