package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Class provides all methods for accessing the database table associated with
 * <b>Users</b> using JDBC.
 *
 * @author Dzmitry Nichyparenka
 */
public class UserDaoJdbc implements UserDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoJdbc.class);
    private static final String LOGGER_USER_ID = "user (id: {})";
    private static final String SQL_CREATE_USER = "INSERT INTO \"public\".users (username, password, first_name, " +
            "last_name) VALUES (?, ?, ?, ?)";
    private static final String SQL_READ_USER = "SELECT username, password, first_name, last_name FROM " +
            "\"public\".users WHERE id=?";
    private static final String SQL_READ_USER_BY_USERNAME = "SELECT id, password, first_name, last_name FROM " +
            "\"public\".users WHERE username=?";
    private static final String SQL_UPDATE_USER_MAIN_DATA = "UPDATE \"public\".users SET password=?, first_name=?, " +
            "last_name=? WHERE username=?";
    private static final String SQL_UPDATE_USER_NAMES = "UPDATE \"public\".users SET first_name=?, last_name=? " +
            "WHERE username=?";
    private static final String SQL_DELETE_USER = "DELETE FROM \"public\".users WHERE id=?";
    private static final String PARAMETER_ID = "id";
    private static final String PARAMETER_USERNAME = "username";
    private static final String PARAMETER_PASSWORD = "password";
    private static final String PARAMETER_FIRST_NAME = "first_name";
    private static final String PARAMETER_LAST_NAME = "last_name";
    private JdbcTemplate jdbcTemplate;

    public UserDaoJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * The method creates the record in the database table with users using JDBC.
     * The new record will be created with data from the object that is passed to the
     * parameter of this method.
     *
     * @param user an entity filled with data to create a record in the database
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(User user) {
        String username = user.getUsername();
        String password = user.getPassword();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        if (jdbcTemplate.update(SQL_CREATE_USER, username, password, firstName, lastName) == 1) {
            LOGGER.info("user (username: {}, firstName: {}, lastName: {})", username, firstName, lastName);
            return true;
        }
        LOGGER.error("user (username: {}, firstName: {}, lastName: {})", username, firstName, lastName);
        return false;
    }

    /**
     * The method reads the record from the database table with users using JDBC.
     *
     * @param id user record id
     * @return an entity with data
     */
    @Override
    public User read(long id) {
        return jdbcTemplate.queryForObject(SQL_READ_USER, new Object[]{id}, (resultSet, rowNum) -> {
            String username = resultSet.getString(PARAMETER_USERNAME);
            String password = resultSet.getString(PARAMETER_PASSWORD);
            String firstName = resultSet.getString(PARAMETER_FIRST_NAME);
            String lastName = resultSet.getString(PARAMETER_LAST_NAME);

            User user = User.builder()
                    .id(id)
                    .username(username)
                    .password(password)
                    .firstName(firstName)
                    .lastName(lastName)
                    .build();
            LOGGER.info("user (id: {}, username: {}, firstName: {}, lastName: {})",
                    id, username, firstName, lastName);
            return user;
        });
    }

    /**
     * The method updates the record in the database table with users using JDBC.
     *
     * @param user an entity to update the record
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(User user) {
        long id = user.getId();
        String username = user.getUsername();
        String password = user.getPassword();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        if (password != null && !password.isEmpty()) {
            if (jdbcTemplate.update(SQL_UPDATE_USER_MAIN_DATA, password, firstName, lastName, username) == 1) {
                LOGGER.info("user (id: {}, username: {}, firstName: {}, lastName: {})",
                        id, username, firstName, lastName);
                return true;
            }
        } else {
            if (jdbcTemplate.update(SQL_UPDATE_USER_NAMES, firstName, lastName, username) == 1) {
                LOGGER.info("user (id: {}, username: {}, firstName: {}, lastName: {})",
                        id, username, firstName, lastName);
                return true;
            }
        }
        LOGGER.error(LOGGER_USER_ID, id);
        return false;
    }

    /**
     * The method deletes the record from the database table with users using JPA
     * implementation.
     *
     * @param id user record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (jdbcTemplate.update(SQL_DELETE_USER, id) == 1) {
            LOGGER.info(LOGGER_USER_ID, id);
            return true;
        }
        LOGGER.error(LOGGER_USER_ID, id);
        return false;
    }

    /**
     * The method reads the record from the database table with users by username
     * using JDBC.
     *
     * @param username username
     * @return user entity
     */
    @Override
    public User readByUsername(String username) {
        return jdbcTemplate.queryForObject(SQL_READ_USER_BY_USERNAME, new Object[]{username}, (resultSet, rowNum) -> {
            long id = resultSet.getLong(PARAMETER_ID);
            String password = resultSet.getString(PARAMETER_PASSWORD);
            String firstName = resultSet.getString(PARAMETER_FIRST_NAME);
            String lastName = resultSet.getString(PARAMETER_LAST_NAME);

            return User.builder()
                    .id(id)
                    .username(username)
                    .password(password)
                    .firstName(firstName)
                    .lastName(lastName)
                    .build();
        });
    }
}
