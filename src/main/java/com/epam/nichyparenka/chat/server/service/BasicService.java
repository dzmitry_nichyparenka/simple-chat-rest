package com.epam.nichyparenka.chat.server.service;

import com.epam.nichyparenka.chat.server.dto.BasicDto;

/**
 * The interface provides abstract methods for all services with the main logic
 * in the application. All abstract methods must be implemented by all classes,
 * which are intended to play the role of services. This is the main interface
 * of the service layer.
 *
 * @param <E> the Data Transfer Object
 * @author Dzmitry Nichyparenka
 */
public interface BasicService<E extends BasicDto> {

    /**
     * The method creates new record in the data storage in accordance with the data in the
     * provided Data Transfer Object. For this purpose, an object of the appropriate type from
     * DAO layer is used.
     *
     * @param dto the Data Transfer Object, filled with data to create
     * @return true if the creation was successful, false otherwise
     */
    boolean create(E dto);

    /**
     * The method reads the data from the database in accordance with the provided identifier.
     * For this purpose, an object of the appropriate type from DAO layer is used.
     *
     * @param id record id
     * @return the data transfer object filled with data
     */
    E read(long id);

    /**
     * The method updates the data of the object in the database in accordance
     * with the provided identifier from the DTO. For this purpose, an object
     * of the appropriate type from DAO layer is used.
     *
     * @param dto the Data Transfer Object to update the record in the database
     * @return true if the update was successful, false otherwise
     */
    boolean update(E dto);

    /**
     * The method deletes the data from the database in accordance with the provided
     * identifier. For this purpose, an object of the appropriate type from DAO layer
     * is used.
     *
     * @param id record id
     * @return true if the deletion was successful, false otherwise
     */
    boolean delete(long id);
}
