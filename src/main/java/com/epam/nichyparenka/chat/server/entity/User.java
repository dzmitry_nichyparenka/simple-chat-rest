package com.epam.nichyparenka.chat.server.entity;

import lombok.*;

/**
 * The class represents the entity which contains all data of the user.
 *
 * @author Dzmitry Nichyparenka
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends BasicEntity {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Chat chat;

    /**
     * The constructor creates user entity with id.
     *
     * @param id id of the user
     */
    public User(long id) {
        super(id);
    }

    /**
     * The constructor creates the fully completed user entity.
     *
     * @param id        id of the user
     * @param username  username
     * @param password  password of the user
     * @param firstName first name of the user
     * @param lastName  last name of the user
     */
    @Builder
    public User(long id, String username, String password, String firstName, String lastName) {
        super(id);
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
