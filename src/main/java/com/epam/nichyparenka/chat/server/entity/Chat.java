package com.epam.nichyparenka.chat.server.entity;

import lombok.*;
import java.util.List;

/**
 * The class represents the entity which contains all data of the chat.
 *
 * @author Dzmitry Nichyparenka
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Chat extends BasicEntity {
    private String name;
    private List<User> users;
    private List<Message> messages;

    /**
     * The constructor creates chat entity with id.
     *
     * @param id id of the chat
     */
    public Chat(long id) {
        super(id);
    }

    /**
     * The constructor creates the fully completed chat entity.
     *
     * @param id   id of the chat
     * @param name name of the chat
     */
    @Builder
    public Chat(long id, String name) {
        super(id);
        this.name = name;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }
}
