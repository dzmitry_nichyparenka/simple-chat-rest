package com.epam.nichyparenka.chat.server.dao;

import com.epam.nichyparenka.chat.server.entity.ChatMember;

import java.util.List;

/**
 * The interface contains all necessary abstract methods (basic CRUD and
 * other necessary) and it must be implemented by all classes which are
 * intended to play the role of DAOs for Chat Members.
 *
 * @author Dzmitry Nichyparenka
 */
public interface ChatMemberDao extends BasicDao<ChatMember> {

    /**
     * The implementation of the method reads all records from the database table
     * with chat members, which are belong to a specific chat.
     *
     * @param chatId chat record id
     * @return list of entities of certain type
     */
    List readAllByChatId(long chatId);
}
