package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.dao.MessageDao;
import com.epam.nichyparenka.chat.server.entity.Chat;
import com.epam.nichyparenka.chat.server.entity.Message;
import com.epam.nichyparenka.chat.server.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Class provides all methods for accessing the database table associated with
 * <b>Messages</b> using JDBC.
 *
 * @author Dzmitry Nichyparenka
 */
public class MessageDaoJdbc implements MessageDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageDaoJdbc.class);
    private static final String SQL_CREATE_MESSAGE = "INSERT INTO \"public\".messages (sender_id, chat_id, text, " +
            "time) VALUES (?, ?, ?, now())";
    private static final String SQL_READ_MESSAGE = "SELECT sender_id, chat_id, text, time FROM \"public\".messages " +
            "WHERE id=?";
    private static final String SQL_READ_ALL_MESSAGES = "SELECT id, sender_id, text, time FROM \"public\".messages " +
            "WHERE chat_id=?";
    private static final String SQL_UPDATE_MESSAGE = "UPDATE \"public\".messages SET sender_id=?, chat_id=?, time=?, " +
            "text=? WHERE id=?";
    private static final String SQL_DELETE_MESSAGE = "DELETE FROM \"public\".messages WHERE id=?";
    private static final String PARAMETER_ID = "id";
    private static final String PARAMETER_CHAT_ID = "chat_id";
    private static final String PARAMETER_SENDER_ID = "sender_id";
    private static final String PARAMETER_TEXT = "text";
    private static final String PARAMETER_TIME = "time";
    private JdbcTemplate jdbcTemplate;

    public MessageDaoJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * The method creates the record in the database table with messages using JDBC.
     * The new record will be created with data from the object that is passed to the
     * parameter of this method.
     *
     * @param message an entity filled with data to create a record in the database
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(Message message) {
        User sender = message.getSender();
        Chat chat = message.getChat();
        long senderId = sender.getId();
        long chatId = chat.getId();
        String text = message.getText();

        if (jdbcTemplate.update(SQL_CREATE_MESSAGE, senderId, chatId, text) == 1) {
            LOGGER.info("message (senderId: {}, chatId: {}, text: {})", senderId, chatId, text);
            return true;
        }
        LOGGER.error("message (senderId: {}, chatId: {}, text: {})", senderId, chatId, text);
        return false;
    }

    /**
     * The method reads the record from the database table with messages using JDBC.
     *
     * @param id message record id
     * @return an entity with data
     */
    @Override
    public Message read(long id) {
        return jdbcTemplate.queryForObject(SQL_READ_MESSAGE, new Object[]{id}, (resultSet, rowNumMessage) -> {
            long chatId = resultSet.getLong(PARAMETER_CHAT_ID);
            long senderId = resultSet.getLong(PARAMETER_SENDER_ID);
            String text = resultSet.getString(PARAMETER_TEXT);
            LocalDateTime time = resultSet.getTimestamp(PARAMETER_TIME).toLocalDateTime();

            Message message = Message.builder()
                    .id(id)
                    .chat(new Chat(chatId))
                    .sender(new User(senderId))
                    .text(text)
                    .time(time)
                    .build();
            LOGGER.info("message (id: {}, chatId: {}, senderId: {}, time: {}, text: {})", id, chatId, senderId,
                    text, time);
            return message;
        });
    }

    /**
     * The method reads all records from the database table with messages,
     * which are belong to a specific chat.
     *
     * @param chatId chat record id
     * @return list of messages found
     */
    @Override
    public List<Message> readAllByChatId(long chatId) {
        List<Message> messages = jdbcTemplate.query(SQL_READ_ALL_MESSAGES, new Object[]{chatId}, (resultSet, rowNum) -> {
            long messageId = resultSet.getLong(PARAMETER_ID);
            long senderId = resultSet.getLong(PARAMETER_SENDER_ID);
            String text = resultSet.getString(PARAMETER_TEXT);
            LocalDateTime time = resultSet.getTimestamp(PARAMETER_TIME).toLocalDateTime();

            return Message.builder()
                    .id(messageId)
                    .sender(new User(senderId))
                    .chat(new Chat(chatId))
                    .text(text)
                    .time(time)
                    .build();
        });
        LOGGER.info("messages (count: {})", messages.size());
        return messages;
    }

    /**
     * The method updates the record in the database table with messages using JDBC.
     *
     * @param message an entity to update the record
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(Message message) {
        User sender = message.getSender();
        Chat chat = message.getChat();
        long id = message.getId();
        long senderId = sender.getId();
        long chatId = chat.getId();
        String text = message.getText();
        LocalDateTime time = message.getTime();

        if (jdbcTemplate.update(SQL_UPDATE_MESSAGE, senderId, chatId, time, text, id) == 1) {
            LOGGER.info("message (id: {}, chatId: {}, senderId: {}, text: {})", id, chatId, senderId, text);
            return true;
        }
        LOGGER.error("message (id: {}, chatId: {}, senderId: {}, text: {})", id, chatId, senderId, text);
        return false;
    }

    /**
     * The method deletes the record from the database table with messages using JPA
     * implementation.
     *
     * @param id message record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (jdbcTemplate.update(SQL_DELETE_MESSAGE, id) == 1) {
            LOGGER.info("message (id: {})", id);
            return true;
        }
        LOGGER.error("message (id: {})", id);
        return false;
    }
}
