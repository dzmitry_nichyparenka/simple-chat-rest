package com.epam.nichyparenka.chat.server.service;

import com.epam.nichyparenka.chat.server.dto.ChatMemberDto;

import java.util.List;

/**
 * The interface provides specific abstract methods for the service,
 * which contains the main logic associated with chat members in the
 * application.
 *
 * @author Dzmitry Nichyparenka
 */
public interface ChatMemberService extends BasicService<ChatMemberDto> {

    /**
     * The implementation of the method reads all chat members in the specified chat.
     *
     * @param chatId chat record id
     * @return list of found chat members
     */
    List<ChatMemberDto> readAllByChatId(long chatId);

}
