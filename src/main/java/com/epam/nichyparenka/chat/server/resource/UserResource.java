package com.epam.nichyparenka.chat.server.resource;

import com.epam.nichyparenka.chat.server.dto.ChatDto;
import com.epam.nichyparenka.chat.server.dto.UserDto;
import com.epam.nichyparenka.chat.server.service.ChatService;
import com.epam.nichyparenka.chat.server.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Class handles requests from clients associated with users.
 *
 * @author Dzmitry Nichyparenka
 */
@Path("users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);
    private static final String REGEX_HEADER_DELIMITER = " ";
    private static final String REGEX_USER_DATA_DELIMITER = ":";
    private final UserService userService;
    private final ChatService chatService;

    @HeaderParam("authorization")
    private String authorization;

    @Autowired
    public UserResource(UserService userService, ChatService chatService) {
        this.userService = userService;
        this.chatService = chatService;
    }

    @GET
    @Path("{userId}")
    public Response getUser(@PathParam("userId") long userId) {
        if (authenticateUser()) {
            UserDto user = userService.read(userId);
            LOGGER.info("[GET] users {}", user);
            return Response.ok(user).build();
        }
        LOGGER.error("[GET] users - unauthorized");
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    public Response getUserByUsername(@QueryParam("username") String username) {
        if (authenticateUser()) {
            UserDto user = userService.readNames(username);
            LOGGER.info("[GET] users {}", user);
            return Response.ok(user).build();
        }
        LOGGER.error("[GET] chats - unauthorized");
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    @Path("{userId}/chats")
    public Response getChatsByUsername(@PathParam("userId") long userId) {
        if (authenticateUser()) {
            List<ChatDto> chatsDTOs = chatService.readChatsByUserId(userId);
            LOGGER.info("[GET] chats {}", chatsDTOs.size());
            return Response.ok(chatsDTOs).build();
        }
        LOGGER.error("[GET] chats - unauthorized");
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @POST
    public Response createUser(@RequestBody UserDto userDto) {
        if (authenticateUser()) {
            if (userService.create(userDto)) {
                LOGGER.info("[POST] users {}", userDto);
                return Response.status(Response.Status.CREATED).build();
            }
            LOGGER.error("[POST] users {}", userDto);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        LOGGER.error("[POST] users - unauthorized");
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @POST
    @Path("account")
    @Produces(MediaType.TEXT_PLAIN)
    public Response checkUser() {
        if (authenticateUser()) {
            LOGGER.info("[POST] users");
            return Response.ok(authorization.split(REGEX_HEADER_DELIMITER)[1]).build();
        }
        LOGGER.error("[POST] users - unauthorized");
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @PUT
    @Path("{username}")
    public Response updateUser(@RequestBody UserDto userDto) {
        if (authenticateUser()) {
            if (userService.update(userDto)) {
                LOGGER.info("[PUT] users {}", userDto);
                userDto.setPassword(null);
                return Response.ok().entity(userDto).build();
            }
            LOGGER.error("[PUT] users {}", userDto);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        LOGGER.error("[PUT] users - unauthorized");
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    private boolean authenticateUser() {
        String[] headerValueParts = authorization.split(REGEX_HEADER_DELIMITER);
        if (headerValueParts.length == 2 && "Basic".equals(headerValueParts[0])) {
            byte[] userDataBytes = DatatypeConverter.parseBase64Binary(headerValueParts[1]);
            String userDataString = new String(userDataBytes, StandardCharsets.UTF_8);
            String[] userDataParts = userDataString.split(REGEX_USER_DATA_DELIMITER);

            return userService.checkUsernameAndPassword(userDataParts[0], userDataParts[1]);
        }
        return false;
    }
}
