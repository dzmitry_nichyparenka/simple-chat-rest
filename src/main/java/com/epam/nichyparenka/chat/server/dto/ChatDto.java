package com.epam.nichyparenka.chat.server.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data Transfer Object for chats.
 *
 * @author Dzmitry Nichyparenka
 */
@Data
@NoArgsConstructor
@XmlRootElement(name = "Chat")
public class ChatDto implements BasicDto {
    private long chatId;
    private String chatName;
    private int membersCount;

    /**
     * The constructor creates chat DTO with id.
     *
     * @param chatId id of the chat
     */
    public ChatDto(long chatId) {
        this.chatId = chatId;
    }

    /**
     * The constructor creates the fully completed chat DTO.
     *
     * @param chatId       id of the chat
     * @param chatName     name of the chat
     * @param membersCount count of the chat members
     */
    @Builder
    public ChatDto(long chatId, String chatName, int membersCount) {
        this.chatId = chatId;
        this.chatName = chatName;
        this.membersCount = membersCount;
    }
}
