package com.epam.nichyparenka.chat.server.service;

import com.epam.nichyparenka.chat.server.dto.UserDto;

/**
 * The interface provides specific abstract methods for the service,
 * which contains the main logic associated with users in the application.
 *
 * @author Dzmitry Nichyparenka
 */
public interface UserService extends BasicService<UserDto> {

    /**
     * Checks the username and password correctness in the database.
     *
     * @param username username
     * @param password password of the user
     * @return true, if the username and password is correct, false otherwise
     */
    boolean checkUsernameAndPassword(String username, String password);

    /**
     * Reads user first and last names.
     *
     * @param username username
     * @return user with data
     */
    UserDto readNames(String username);
}
