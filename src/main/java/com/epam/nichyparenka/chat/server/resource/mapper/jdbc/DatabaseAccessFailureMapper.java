package com.epam.nichyparenka.chat.server.resource.mapper.jdbc;

import org.springframework.dao.DataAccessResourceFailureException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Mapper for situations, when it is impossible to connect to the database due to incorrect
 * connection data or problems on the database side.
 *
 * @author Dzmitry Nichyparenka
 */
@Provider
public class DatabaseAccessFailureMapper implements ExceptionMapper<DataAccessResourceFailureException> {
    private static final String MESSAGE = "Action failed due to a problem with the connection to the database.";

    @Override
    public Response toResponse(DataAccessResourceFailureException exception) {
        // HTTP status code 503 - Service Unavailable
        return Response.status(503).entity(MESSAGE).type(MediaType.TEXT_PLAIN).build();
    }
}
