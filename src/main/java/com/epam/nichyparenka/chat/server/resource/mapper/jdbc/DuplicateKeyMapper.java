package com.epam.nichyparenka.chat.server.resource.mapper.jdbc;

import org.springframework.dao.DuplicateKeyException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Mapper for situations, when the record in the database can't be created or updated due to
 * the fact that unique data already exists.
 *
 * @author Dzmitry Nichyparenka
 */
@Provider
public class DuplicateKeyMapper implements ExceptionMapper<DuplicateKeyException> {
    private static final String MESSAGE = "Action failed due to unique data duplication.";

    @Override
    public Response toResponse(DuplicateKeyException exception) {
        // HTTP status code 422 - Unprocessable Entity
        return Response.status(422).entity(MESSAGE).type(MediaType.TEXT_PLAIN).build();
    }
}
