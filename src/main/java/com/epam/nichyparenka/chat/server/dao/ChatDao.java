package com.epam.nichyparenka.chat.server.dao;

import com.epam.nichyparenka.chat.server.entity.Chat;

import java.util.List;

/**
 * The interface contains all necessary abstract methods (basic CRUD and
 * other necessary) and it must be implemented by all classes which are
 * intended to play the role of DAOs for Chats.
 *
 * @author Dzmitry Nichyparenka
 */
public interface ChatDao extends BasicDao<Chat> {

    /**
     * The implementation of the method reads all chats by username.
     *
     * @param userId id of the user
     * @return list of chats
     */
    List<Chat> readAllByUserId(long userId);
}
