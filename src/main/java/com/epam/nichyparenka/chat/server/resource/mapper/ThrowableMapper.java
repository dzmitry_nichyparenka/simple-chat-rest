package com.epam.nichyparenka.chat.server.resource.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Mapper for situations, when unhandled exception was thrown.
 *
 * @author Dzmitry Nichyparenka
 */
@Provider
public class ThrowableMapper implements ExceptionMapper<Throwable> {
    private static final String MESSAGE = "Something went wrong (unknown error).";

    @Override
    public Response toResponse(Throwable exception) {
        // 520 - unknown
        return Response.status(520).entity(MESSAGE).build();
    }
}
