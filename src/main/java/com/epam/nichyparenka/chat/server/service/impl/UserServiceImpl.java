package com.epam.nichyparenka.chat.server.service.impl;

import com.epam.nichyparenka.chat.server.dao.UserDao;
import com.epam.nichyparenka.chat.server.dto.UserDto;
import com.epam.nichyparenka.chat.server.entity.User;
import com.epam.nichyparenka.chat.server.service.UserService;
import com.epam.nichyparenka.chat.server.util.DtoConverter;
import com.epam.nichyparenka.chat.server.util.EntityConverter;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 * Class provides methods with the main logic associated with users in the application.
 *
 * @author Dzmitry Nichyparenka
 */
public class UserServiceImpl implements UserService {
    private static final int HASHING_SALT_LOG_ROUNDS = 4;
    private UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * The method creates a new user in accordance with the data in the provided DTO.
     * For this purpose, {@code UserDao} object is used.
     *
     * @param userDto the Data Transfer Object filled with data to create a new user
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(UserDto userDto) {
        User user = DtoConverter.convertUserDtoToEntity(userDto);
        String hashedPassword = generatePasswordHash(user.getPassword());
        user.setPassword(hashedPassword);
        return userDao.create(user);
    }

    /**
     * The method reads the user in accordance with the provided identifier.
     * For this purpose, {@code UserDao} object is used.
     *
     * @param id user record id
     * @return the Data Transfer Object filled with data of user
     */
    @Override
    public UserDto read(long id) {
        User user = userDao.read(id);
        return EntityConverter.convertUserEntityToDTO(user);
    }

    /**
     * The method updates the data of the user by id.
     * For this purpose, {@code UserDao} object is used.
     *
     * @param userDto the Data Transfer Object to update the user
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(UserDto userDto) {
        String firstName = userDto.getFirstName();
        String lastName = userDto.getLastName();
        if (firstName == null || lastName == null || firstName.isEmpty() || lastName.isEmpty()) {
            throw new DataIntegrityViolationException("Unable to update");
        }

        User user = DtoConverter.convertUserDtoToEntity(userDto);
        String password = user.getPassword();
        if (password != null && !password.isEmpty()) {
            user.setPassword(generatePasswordHash(user.getPassword()));
        }
        if (!userDao.update(user)) {
            throw new EmptyResultDataAccessException(1);
        }
        return true;
    }

    /**
     * The method deletes the user in accordance with the provided identifier.
     * For this purpose, {@code UserDao} object is used.
     *
     * @param id user record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (userDao.delete(id)) {
            return true;
        } else {
            throw new EmptyResultDataAccessException(1);
        }
    }

    /**
     * The method checks the username and password correctness in the database.
     *
     * @param username username
     * @param password password of the user
     * @return true, if the username and password is correct, false otherwise
     */
    @Override
    public boolean checkUsernameAndPassword(String username, String password) {
        try {
            User user = userDao.readByUsername(username);
            return BCrypt.checkpw(password, user.getPassword());
        } catch (EmptyResultDataAccessException exception) {
            return false;
        }
    }

    /**
     * The method reads the first name and last name of the user.
     *
     * @param username username
     * @return user entity
     */
    @Override
    public UserDto readNames(String username) {
        User user = userDao.readByUsername(username);
        return EntityConverter.convertUserEntityToDTO(user);
    }

    /**
     * The method generates hash for the password.
     *
     * @param password password
     * @return hashed password of the user
     */
    private String generatePasswordHash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(HASHING_SALT_LOG_ROUNDS));
    }
}
