package com.epam.nichyparenka.chat.server.service;

import com.epam.nichyparenka.chat.server.dto.ChatDto;

import java.util.List;

/**
 * The interface provides specific abstract methods for the service,
 * which contains the main logic associated with chats in the application.
 *
 * @author Dzmitry Nichyparenka
 */
public interface ChatService extends BasicService<ChatDto> {

    /**
     * The implementation of the method reads all chats by username.
     *
     * @param userId id of the user
     * @return list of found DTOs
     */
    List<ChatDto> readChatsByUserId(long userId);
}
