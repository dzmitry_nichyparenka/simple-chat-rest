package com.epam.nichyparenka.chat.server.dao.jdbc;

import com.epam.nichyparenka.chat.server.dao.ChatDao;
import com.epam.nichyparenka.chat.server.entity.Chat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * Class provides all methods for accessing the database table associated with
 * <b>Chats</b> using JDBC.
 *
 * @author Dzmitry Nichyparenka
 */
public class ChatDaoJdbc implements ChatDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatDaoJdbc.class);
    private static final String SQL_CREATE_CHAT = "INSERT INTO \"public\".chats (name) VALUES (?)";
    private static final String SQL_READ_CHAT = "SELECT name FROM \"public\".chats WHERE id=?";
    private static final String SQL_READ_ALL_CHATS_BY_USERNAME = "SELECT DISTINCT \"public\".chats.id, " +
            "\"public\".chats.name FROM \"public\".chats INNER JOIN \"public\".chat_members ON \"public\".chats.id=" +
            "\"public\".chat_members.chat_id WHERE \"public\".chat_members.user_id=?";
    private static final String SQL_UPDATE_CHAT = "UPDATE \"public\".chats SET name=? WHERE id=?";
    private static final String SQL_DELETE_CHAT = "DELETE FROM \"public\".chats WHERE id=?";
    private static final String PARAMETER_ID = "id";
    private static final String PARAMETER_NAME = "name";
    private JdbcTemplate jdbcTemplate;

    public ChatDaoJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * The method creates the record in the database table with users using JDBC.
     * The new record will be created with data from the object that is passed
     * to the parameter of this method.
     *
     * @param chat an entity filled with data to create a record in the database
     * @return true if the creation was successful, false otherwise
     */
    @Override
    public boolean create(Chat chat) {
        String name = chat.getName();

        if (jdbcTemplate.update(SQL_CREATE_CHAT, name) == 1) {
            LOGGER.info("chat (name: {})", name);
            return true;
        }
        LOGGER.error("chat (name: {})", name);
        return false;
    }

    /**
     * The method reads the record from the database table with chats using JDBC.
     *
     * @param id chat record id
     * @return an entity with data
     */
    @Override
    public Chat read(long id) {
        return jdbcTemplate.queryForObject(SQL_READ_CHAT, new Object[]{id}, (resultSet, rowNum) -> {
            String name = resultSet.getString(PARAMETER_NAME);

            Chat chat = Chat.builder()
                    .id(id)
                    .name(name)
                    .build();
            LOGGER.info("chat (id: {}, name: {})", id, name);
            return chat;
        });
    }

    /**
     * The method updates the record in the database table with chats using JDBC.
     *
     * @param chat an entity to update the record
     * @return true if the update was successful, false otherwise
     */
    @Override
    public boolean update(Chat chat) {
        long id = chat.getId();
        String name = chat.getName();

        if (jdbcTemplate.update(SQL_UPDATE_CHAT, name, id) == 1) {
            LOGGER.info("chat (id: {}, name: {})", id, name);
            return true;
        }
        return false;
    }

    /**
     * The method deletes the record from the database table with chats using JDBC.
     *
     * @param id chat record id
     * @return true if the deletion was successful, false otherwise
     */
    @Override
    public boolean delete(long id) {
        if (jdbcTemplate.update(SQL_DELETE_CHAT, id) == 1) {
            LOGGER.info("chat (id: {})", id);
            return true;
        }
        LOGGER.error("chat (id: {})", id);
        return false;
    }

    /**
     * The method reads all chats by username using JDBC.
     *
     * @param userId user id
     * @return list of chats
     */
    @Override
    public List<Chat> readAllByUserId(long userId) {
        List<Chat> chats = jdbcTemplate.query(SQL_READ_ALL_CHATS_BY_USERNAME, new Object[]{userId},
                (resultSet, rowNum) -> {
                    long id = resultSet.getLong(PARAMETER_ID);
                    String name = resultSet.getString(PARAMETER_NAME);

                    return new Chat(id, name);
                });
        LOGGER.info("chats (count: {})", chats.size());
        return chats;
    }
}
