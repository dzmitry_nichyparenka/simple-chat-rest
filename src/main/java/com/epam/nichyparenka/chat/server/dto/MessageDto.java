package com.epam.nichyparenka.chat.server.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

/**
 * Data Transfer Object for messages.
 *
 * @author Dzmitry Nichyparenka
 */
@Data
@NoArgsConstructor
@XmlRootElement(name = "Message")
public class MessageDto implements BasicDto {
    private long messageId;
    private long senderId;
    private long chatId;
    private String text;
    private LocalDateTime time;
    private String username;
    private String chatName;

    /**
     * The constructor creates message DTO with id.
     *
     * @param messageId id of the message
     */
    public MessageDto(long messageId) {
        this.messageId = messageId;
    }

    /**
     * The constructor creates the fully completed message DTO.
     *
     * @param messageId id of the message
     * @param senderId  id of the sender
     * @param chatId    id of the chat
     * @param text      text of the message
     * @param time      time where message was sent
     * @param username  username
     * @param chatName  name of the chat
     */
    @Builder
    public MessageDto(long messageId, long senderId, long chatId, String text, LocalDateTime time, String username,
                      String chatName) {
        this.messageId = messageId;
        this.senderId = senderId;
        this.chatId = chatId;
        this.text = text;
        this.time = time;
        this.username = username;
        this.chatName = chatName;
    }
}
