package com.epam.nichyparenka.chat.server.dto;

import com.epam.nichyparenka.chat.server.entity.UserRole;
import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data Transfer Object for chat members.
 *
 * @author Dzmitry Nichyparenka
 */
@Data
@NoArgsConstructor
@XmlRootElement(name = "ChatMember")
public class ChatMemberDto implements BasicDto {
    private long chatMemberId;
    private long chatId;
    private long userId;
    private UserRole role;
    private String chatName;
    private String username;
    private String firstName;
    private String lastName;

    /**
     * The constructor creates chat member DTO with id.
     *
     * @param chatMemberId id of the chat member
     */
    public ChatMemberDto(long chatMemberId) {
        this.chatMemberId = chatMemberId;
    }

    /**
     * The constructor creates the fully completed chat member DTO.
     *
     * @param chatMemberId id of the chat member
     * @param chatId       id of the chat
     * @param userId       id of the user
     * @param role         role of the user
     * @param chatName     name of the chat
     * @param username     username
     * @param firstName    first name of the user
     * @param lastName     last name of the user
     */
    @Builder
    public ChatMemberDto(long chatMemberId, long chatId, long userId, UserRole role, String chatName, String username,
                         String firstName, String lastName) {
        this.chatMemberId = chatMemberId;
        this.chatId = chatId;
        this.userId = userId;
        this.role = role;
        this.chatName = chatName;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
